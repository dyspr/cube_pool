var colors = {
  light: [13, 13, 13],
  dark: [0, 0, 0]
}

var boardSize
var dimension = 15
var initSize = 0.05

function setup() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)
}

function draw() {
  createCanvas(windowWidth, windowHeight)
  background(colors.light)
  rectMode(CENTER)
  colorMode(RGB, 255, 255, 255, 1)

  fill(colors.dark)
  noStroke()
  rect(windowWidth *  0.5, windowHeight * 0.5, boardSize, boardSize)

  push()
  translate(boardSize * initSize * 0.25 * cos(frameCount * 0.017), boardSize * initSize * 0.25 * sin(frameCount * 0.023))
  for (var i = 0; i < dimension; i++) {
    for (var j = 0; j < dimension; j++) {
      if (i < 3 || i > dimension - 1 - 3 || j < 3 || j > dimension - 1 - 3) {
        noFill()
        stroke(255)
        strokeWeight(boardSize * initSize * 0.05)
        push()
        translate(windowWidth * 0.5 + (i - Math.floor(dimension * 0.5)) * boardSize * initSize, windowHeight * 0.5 + (j - Math.floor(dimension * 0.5)) * boardSize * initSize)
        rect(0, 0, boardSize * initSize, boardSize * initSize)
        pop()
      }
    }
  }
  pop()

  push()
  translate(boardSize * initSize * 0.25 * sin(frameCount * 0.0171), boardSize * initSize * 0.25 * cos(frameCount * 0.0232))
  for (var i = 0; i < dimension - 6; i++) {
    for (var j = 0; j < dimension - 6; j++) {
      noFill()
      stroke(255)
      strokeWeight(boardSize * initSize * 0.05)
      push()
      translate(windowWidth * 0.5 + (i - Math.floor((dimension - 6) * 0.5)) * boardSize * initSize * 0.8, windowHeight * 0.5 + (j - Math.floor((dimension - 6) * 0.5)) * boardSize * initSize * 0.8)
      rect(0, 0, boardSize * initSize * 0.8, boardSize * initSize * 0.8)
      pop()
    }
  }
  pop()

  for (var i = 0; i < dimension - 5; i++) {
    noFill()
    stroke(255)
    strokeWeight(boardSize * initSize * 0.05)
    line(windowWidth * 0.5 + boardSize * initSize * 0.25 * cos(frameCount * 0.017) + ((i + 2.5) - Math.floor(dimension * 0.5)) * boardSize * initSize, windowHeight * 0.5 + boardSize * initSize * 0.25 * sin(frameCount * 0.023) + (2.5 - Math.floor(dimension * 0.5)) * boardSize * initSize, windowWidth * 0.5 + boardSize * initSize * 0.25 * sin(frameCount * 0.0171) + ((i + 2.5) - Math.floor(dimension * 0.5)) * boardSize * initSize * 0.8, windowHeight * 0.5 + boardSize * initSize * 0.25 * cos(frameCount * 0.0232) + (2.5 - Math.floor(dimension * 0.5)) * boardSize * initSize * 0.8)

    line(windowWidth * 0.5 + boardSize * initSize * 0.25 * cos(frameCount * 0.017) + ((i + 2.5) - Math.floor(dimension * 0.5)) * boardSize * initSize, windowHeight * 0.5 + boardSize * initSize * 0.25 * sin(frameCount * 0.023) - (2.5 - Math.floor(dimension * 0.5)) * boardSize * initSize, windowWidth * 0.5 + boardSize * initSize * 0.25 * sin(frameCount * 0.0171) + ((i + 2.5) - Math.floor(dimension * 0.5)) * boardSize * initSize * 0.8, windowHeight * 0.5 + boardSize * initSize * 0.25 * cos(frameCount * 0.0232) - (2.5 - Math.floor(dimension * 0.5)) * boardSize * initSize * 0.8)

    line(windowWidth * 0.5 + boardSize * initSize * 0.25 * cos(frameCount * 0.017) + (2.5 - Math.floor(dimension * 0.5)) * boardSize * initSize, windowHeight * 0.5 + boardSize * initSize * 0.25 * sin(frameCount * 0.023) + ((i + 2.5) - Math.floor(dimension * 0.5)) * boardSize * initSize, windowWidth * 0.5 + boardSize * initSize * 0.25 * sin(frameCount * 0.0171) + (2.5 - Math.floor(dimension * 0.5)) * boardSize * initSize * 0.8, windowHeight * 0.5 + boardSize * initSize * 0.25 * cos(frameCount * 0.0232) + ((i + 2.5) - Math.floor(dimension * 0.5)) * boardSize * initSize * 0.8)

    line(windowWidth * 0.5 + boardSize * initSize * 0.25 * cos(frameCount * 0.017) - (2.5 - Math.floor(dimension * 0.5)) * boardSize * initSize, windowHeight * 0.5 + boardSize * initSize * 0.25 * sin(frameCount * 0.023) + ((i + 2.5) - Math.floor(dimension * 0.5)) * boardSize * initSize, windowWidth * 0.5 + boardSize * initSize * 0.25 * sin(frameCount * 0.0171) - (2.5 - Math.floor(dimension * 0.5)) * boardSize * initSize * 0.8, windowHeight * 0.5 + boardSize * initSize * 0.25 * cos(frameCount * 0.0232) + ((i + 2.5) - Math.floor(dimension * 0.5)) * boardSize * initSize * 0.8)
  }
}

function windowResized() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)
}
